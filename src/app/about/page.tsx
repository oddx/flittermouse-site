'use client'

import InfoCard from "@/components/InfoCard";
import Nav from "@/components/Nav";

export default () => <>
  <Nav />
  <main className='grid md:grid-rows-2 md:grid-cols-2 gap-x-16 gap-y-32 py-4 md:py-8 xl:py-32 text-center md:text-left md:py-16 mx-4 md:mx-16'>
    <InfoCard
      header='The depth to solve your reactive woes'
      body='With over seven years of experience, I have the knowledge necessary to build scalable web applications from frontend down to the database, from greenfield to production, and from design to first commit. This gives my clients confidence in my abilities to handle whatever their app throws at me.'
    />

    <InfoCard
      header='The breadth to elegantly enhance your app'
      body='Having worked in Fortune 500 enterprise environments all the way down to small startups, I have the experience to augment your application. I’ve worked with monoliths, microservices, multi-tenant monorepos, and cloud solutions. All of this allows me to unflinchingly plug in and begin.'
    />

    <InfoCard
      header='The reliability to deliver your features'
      body='If you’re looking for a resource to take your product across the finish line, consistent, iterative delivery of rock solid features has been the single most defining element of my track record. With this, my clients rest easy and assured that when I ship code, it floats.'
    />

    <InfoCard
      header='The communication to complement your team'
      body='The most common feedback I’ve received from reviews is that my teammates enjoy the energy I bring to the team. I leverage my competencies in communication to ease information exchange, proactively loop in stakeholders, and share out knowledge I’ve gained through accessible write ups.'
    />

  </main>
</>
