import Nav from "@/components/Nav";
import Link from "next/link";
// import Image from 'next/image'
// import enmesh_wings from '../../public/enmesh_wings.jpg'

export default () =>
  <>
    <Nav />
    <main className="min-h-[90vh] min-w-screen bg-[url('/enmesh_wings.jpg')] bg-cover bg-top xl:my-32">
      <section className='max-w-[40vw] py-4 md:py-8 md:py-16'>
        <h1 className='text-5xl font-bold mx-4 md:m-8 lg:m-16 py-4 md:py-8 md:py-16'>Robust React freelancing from zero to production</h1>
        <div className='py-16 md:py-4 lg:py-2'>
          <Link
            className='text-zinc-900 font-light p-1.5 rounded-sm bg-gradient-to-r from-gray-200 via-gray-400 to-gray-600 mx-4 md:mx-8 lg:mx-16'
            href='/contact'>
            Hire Me
          </Link>
        </div>
      </section>
      {/*<section>
        <Image
          src={enmesh_wings}
          width={400}
          height={400}
          alt='Picture of Alex'
        />
      </section> */}
    </main>
  </>

