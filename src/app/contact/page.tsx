'use client'

import Nav from "@/components/Nav";
import { useState } from "react";

export default () => {
  const [body, setBody] = useState('')
  const [phone, setPhone] = useState('')
  const [email, setEmail] = useState('')

  const withPreferredContact = (body, phone, email) =>
    !(phone || email) ? body :
      (phone && !email) ? withPhone :
        (!phone && email) ? withEmail :
          withBoth

  const withPhone = `${body}

Preferred contact:
${phone}`

  const withEmail = `${body}

Preferred contact:
${email}`

  const withBoth = `${body}

Preferred contact:
${email} or ${phone}`

  const handlesubmit = (e) => {
    e.preventDefault()
    window.location.href = `mailto:alternate.appease023@silomails.com?subject=${encodeURI('Services Interest')}&body=${encodeURIComponent(withPreferredContact(body, phone, email))}`
  }

  const handleBodyChange = (e) => setBody(e.target.value)
  const handlePhoneChange = (e) => setPhone(e.target.value)
  const handleEmailChange = (e) => setEmail(e.target.value)

  return <>
    < Nav />
    <main className='contact bg-white text-black rounded p-4 md:p-8 xl:p-16 mx-4 sm:mx-32 my-16'>
      <form className='grid md:grid-cols-2 gap-2 sm:gap-8'>
        <label className='hidden' htmlFor="first-name">First Name</label>
        <input className='p-2 h-8 xl:h-12 xl:text-2xl placeholder-black bg-yellow-200 border-[1px] border-black rounded' id="first-name" type="text" name="first-name" placeholder="First Name*" required />
        <div className='grid sm:grid-cols-3 sm:gap-x-2'>
          <label className='hidden' htmlFor="last-name">Last Name</label>
          <input className='p-2 h-8 xl:h-12 xl:text-2xl placeholder-black sm:col-span-2 bg-yellow-200 border-[1px] border-black rounded' id="last-name" type="text" name="last-name" placeholder="Last Name*" required />
          <button className='h-8 xl:h-12 xl:text-2xl hidden sm:inline bg-gray-300 border-[1px] border-black rounded text-black ' type="submit" onClick={handlesubmit} aria-label="submit message">Submit</button>
        </div>
        <label className='hidden' htmlFor="email">Email</label>
        <input className='p-2 h-8 xl:h-12 xl:text-2xl placeholder-black bg-yellow-200 border-[1px] border-black rounded' id="email" type="email" name="email" placeholder="Email" onChange={handleEmailChange} />
        <label className='hidden' htmlFor="phone-number">Phone Number</label>
        <input className='p-2 h-8 xl:h-12 xl:text-2xl placeholder-black bg-yellow-200 border-[1px] border-black rounded' id="phone-number" type="tel" pattern="[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}" name="phone-number" placeholder="Phone Number" onChange={handlePhoneChange} />
        <label className='hidden' htmlFor="message">Your Message</label>
        <textarea className='p-2 h-32 xl:h-[33vh] xl:h-12 xl:text-2xl placeholder-black rounded sm:col-span-2 resize-none' id="message" name="message" placeholder="Your message" onChange={handleBodyChange}></textarea>
        <button className='h-8 xl:h-12 xl:text-2xl sm:hidden block bg-gray-300 border-[1px] border-black rounded text-black' type="submit" onClick={handlesubmit} aria-label="submit message">Submit</button>
      </form>
    </main>
  </>
}
