import type { Metadata } from "next";
import { Roboto } from "next/font/google";
import "./globals.css";

const inter = Roboto({
  weight: ['100', '300', '400'],
  subsets: ["latin"],
  display: 'swap'
});

export const metadata: Metadata = {
  title: "Flittermouse Tech | React Freelancing",
  description: "Robust React freelancing from zero to production",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="min-h-screen min-w-screen bg-zinc-950 text-white">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
