'use client'

import Nav from "@/components/Nav";
import ChatClient from "@/components/ChatClient";
import { useState } from "react";

export default () => {
  const [history, setHistory] = useState<string[]>([])
  const [message, setMessage] = useState<string>('')
  const [focusedClient, setFocusedClient] = useState<number | undefined>(undefined)

  const handleSubmit = (id: number) => (e) => {
    if (e) { e.preventDefault() }
    message !== '' ? setHistory([...history, format(id, message)]) : undefined
    setMessage('')
  }

  const format = (windowId: number, x: string) => `User${windowId + 1}: ${x}`

  return <>
    <Nav />
    <main className='grid md:grid-cols-2 gap-x-16 gap-y-32 py-4 md:py-8 xl:py-16 mx-4 sm:mx-32 my-16'>
      {[...Array(2).keys()].map(x =>
        <ChatClient
          key={x}
          windowId={x}
          history={history}
          message={focusedClient === x ? message : ''}
          setMessage={setMessage}
          setFocusedClient={setFocusedClient}
          handleSubmit={handleSubmit(x)}
        />
      )}
    </main>
  </>
}
