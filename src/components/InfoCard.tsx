export default ({ header, body }) =>
  <section>
    <h3 className='text-xl font-bold mb-2'>{header}</h3>
    <p>{body}</p>
  </section>
