import routes from "@/utils/routes";
import Link from "next/link";
import React from "react";

export default () =>
  <section className='m-4 md:m-8 font-light flex flex-wrap items-center justify-between'>
    <div className='p-1'>
      <Link href={routes.home}><h2 className='text-4xl italic'>Flittermouse Tech</h2></Link>
    </div>
    <div className='text-3xl flex flex-wrap justify-evenly'>
      <Link href={routes.home} className='mx-2 md:mx-4 lg:mx-8'>Home</Link>
      <Link href={routes.about} className='mx-2 md:mx-4 lg:mx-8'>About</Link>
      <Link href={routes.demos} className='mx-2 md:mx-4 lg:mx-8'>Demos</Link>
      <Link href={routes.contact} className='mx-2 md:mx-4 lg:mx-8'>Contact</Link>
    </div>
  </section>
