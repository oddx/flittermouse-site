export default ({ windowId, history, message, setMessage, setFocusedClient, handleSubmit }) => {
  const format = (xs: string[]) => xs.map((x, i) => <div key={i}>{x}</div>)

  return (
    <main className='chat flex bg-white rounded p-2 min-h-40 sm:min-h-80 md:min-h-[50vh]'>
      <div className='w-full self-end'>
        <section className='text-black'>
          {format(history)}
        </section>
        <form className='text-black grid gap-x-2'>
          <input className='p-1 bg-yellow-200 border-[1px] border-black rounded col-span-4 placeholder-black' type='text' placeholder='Type to chat' name='message' onFocus={_ => setFocusedClient(windowId)} onChange={e => setMessage(e.target.value)} value={message} />
          <input className='p-1 bg-gray-300 border-[1px] border-black rounded col-end-6' type='submit' value='submit' onClick={handleSubmit} />
        </form>
      </div>
    </main>
  )
}
