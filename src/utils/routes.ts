export default {
  home: '/',
  about: '/about',
  demos: '/demos',
  contact: '/contact'
}
